import React from 'react';
import {makeStyles} from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";
import CardActionArea from "@material-ui/core/CardActionArea";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Hidden from "@material-ui/core/Hidden";
import CardMedia from "@material-ui/core/CardMedia";
import {PATH_PROJECTS_SVG} from "../app/AppRoutes";
import LogoSvg from './svg/project_logo.svg';
import {Link} from "react-router-dom";

const projectCards = {
    animatedSvg: {
        title: 'Animated SVG',
        subTitle: 'Plain CSS animation for vector graphics',
        description: 'Set of handcrafted cool motions',
        imageUrl: LogoSvg,
        href: PATH_PROJECTS_SVG,
    },
};

const useStyles = makeStyles({
    card: {
        display: 'flex',
    },
    cardDetails: {
        flex: 1,
    },
    cardMedia: {
        width: 160,
    },
});

function ProjectCard(props) {
    const classes = useStyles();

    return (
        <Grid item key={props.card.title} xs={12} md={6}>
            <CardActionArea component={ Link } to={props.card.href}>
                <Card className={classes.card}>
                    <div className={classes.cardDetails}>
                        <CardContent>
                            <Typography component="h2" variant="h5">
                                {props.card.title}
                            </Typography>
                            <Typography variant="subtitle1" color="textSecondary">
                                {props.card.subTitle}
                            </Typography>
                            <Typography variant="subtitle1" paragraph>
                                {props.card.description}
                            </Typography>
                            <Typography variant="subtitle1" color="primary">
                                Continue...
                            </Typography>
                        </CardContent>
                    </div>
                    <Hidden xsDown>
                        <CardMedia
                            className={classes.cardMedia}
                            src={props.card.imageUrl}
                            component='img'
                            title="Random image"
                        />
                    </Hidden>
                </Card>
            </CardActionArea>
        </Grid>
    );
}

export default function Projects() {
    return (
        <React.Fragment>
            <h2>Projects:</h2>
            <Grid container spacing={4}>
                <ProjectCard card={projectCards.animatedSvg}/>
            </Grid>
        </React.Fragment>
    );
}