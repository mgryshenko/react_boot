import React from 'react';
import {Box, makeStyles} from "@material-ui/core";
import styled, {keyframes} from 'styled-components';
import {ReactComponent as AnimCss} from './anim_css.svg';
import {ReactComponent as AnimSvg} from './anim_svg.svg';
import {ReactComponent as AnimPath} from './anim_path.svg';
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles(theme => ({
    margin: {
        marginTop: theme.spacing(2),
        padding: theme.spacing(2, 2),
        alignItems: 'center',
    },
    container: {
        marginTop: theme.spacing(2),
        padding: theme.spacing(2, 2),
    },
    box: {
        alignItems: 'center',
        width: '200px',
        height: '200px',
    }
}));

const rotate = keyframes`
    from {
        transform: rotate(0deg);
    }
    to {
        transform: rotate(360deg);
    }
`;

const colorFade = keyframes`
    0% {
        fill: #23bfb9;
    }
    50% {
        fill: #9738E0;
    }
    100% {
        fill: #23bfb9;
    }
`;

export const AnimCssImage = styled(AnimCss)`
    animation: ${rotate} infinite 10s linear;
    .outer_rect {
        animation: ${colorFade} infinite 5s linear;
    }
    height: 10rem;
    width: 10rem;
    display: block;
    margin: auto;
`;

export default function AnimatedSvgProject(props) {
    const classes = useStyles();

    return (
        <Grid container className={classes.container} spacing={2}>
            <Grid item md={6} sm={12}>
                <Paper  elevation={2}>
                    <Grid container>
                        <Grid item sm={6} xs={12} className={classes.margin}>
                            <Typography>Pure SVG animation</Typography>
                            <Box className={classes.box}>
                                <AnimSvg/>
                            </Box>
                        </Grid>
                        <Grid item sm={6} xs={12} className={classes.margin}>
                            <Typography>CSS animation</Typography>
                            <Box className={classes.box}>
                                <AnimCssImage/>
                            </Box>
                        </Grid>
                    </Grid>
                </Paper>
            </Grid>
            <Grid item md={6} sm={12} >
                <Paper  elevation={2}>
                    <Grid container>
                        <Grid item sm={6} xs={12} className={classes.margin}>
                            <Typography>SVG path morphing</Typography>
                            <Box className={classes.box}>
                                <AnimPath/>
                            </Box>
                        </Grid>
                        <Grid item sm={6} xs={12} className={classes.margin}>
                            <Typography>CSS morphing</Typography>
                            <Box className={classes.box}>
                                <Typography color='error'>No such CSS equivivalent!</Typography>
                            </Box>
                        </Grid>
                    </Grid>
                </Paper>
            </Grid>
        </Grid>
    );
}