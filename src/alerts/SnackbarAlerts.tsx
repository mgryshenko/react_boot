import React, {useEffect} from 'react';
import {SnackbarProvider, useSnackbar} from "notistack";
import {ALERT_EVENT, EventRegister} from "../app/EventBus";
import CloseIcon from '@material-ui/icons/Close';
import IconButton from "@material-ui/core/IconButton";
import {makeStyles} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {IAlertAction, IAlertModel} from "./AlertModel";

const useStyles = makeStyles({
    white: {
        color: 'white',
    },
});

export interface ISnackbarActionProps {
    snackbarKey: string;
    withClose: boolean;
    actions: Array<IAlertAction>;
}

export const SnackbarActions: React.FC<ISnackbarActionProps> = (props) => {
    const classes = useStyles();
    const { closeSnackbar } = useSnackbar();
    const onClose = () => closeSnackbar(props.snackbarKey);
    const onButtonClick = (callback: () => void) => () => {
        callback();
        onClose();
    };

    return (
        <React.Fragment>
            {props.actions.map(action =>
                <Button className={classes.white} key={action.name} onClick={onButtonClick(action.callback)}>{action.name}</Button>
            )}
            {props.withClose &&
                <IconButton aria-label="Close" title="Close" onClick={onClose}>
                    <CloseIcon className={classes.white} />
                </IconButton>
            }
        </React.Fragment>
    );
};

function SnackbarAlertEventListener() {
    const { enqueueSnackbar } = useSnackbar();

    const onAlert = (data: IAlertModel) => {
        enqueueSnackbar(data.message, data.options);
    };

    useEffect(() => {
        const listener = EventRegister.subscribe(ALERT_EVENT, onAlert);
       return () => EventRegister.unsubscribe(listener);
    });

    return <React.Fragment/>;
}

export default function SnackbarAlerts() {
    return (
        <SnackbarProvider maxSnack={3} hideIconVariant={false} dense>
            <SnackbarAlertEventListener/>
        </SnackbarProvider>
    );
}