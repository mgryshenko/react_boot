import React from "react";
import {SnackbarActions} from "./SnackbarAlerts"
import {OptionsObject, VariantType} from "notistack";
import {SnackbarOrigin} from "@material-ui/core/Snackbar";

export interface IAlertAction {
    readonly name: string,
    readonly callback: () => void;
}
export interface IAlertModel {
    readonly message: string;
    readonly options: OptionsObject;
}

class AlertBuilder {
    readonly message: string = '';
    private variant: VariantType = 'default';
    private persist: boolean = false;
    private vertical: SnackbarOrigin["vertical"] = 'top';
    private horizontal: SnackbarOrigin["horizontal"] = 'right';
    private withCloseButton: boolean = true;
    private actions: Array<IAlertAction> = [];

    constructor(msg: string) {
        this.message = msg;
    }

    withVariant(variant: AlertBuilder["variant"]) {
        this.variant = variant;
        return this;
    }
    withPersist() {
        this.persist = true;
        return this;
    }
    withPosition(ver: AlertBuilder["vertical"], hor: AlertBuilder["horizontal"]) {
        this.vertical = ver;
        this.horizontal = hor;
        return this;
    }
    withAction(name: IAlertAction["name"], callback: IAlertAction["callback"]) {
        this.actions.push({name, callback});
        return this;
    }
    withoutClose() {
        this.withCloseButton = false;
        return this;
    }

    build(): IAlertModel {
        const action = this.createSnackbarActions(this.withCloseButton, this.actions);
        return {
            message: this.message,
            options: {
                variant: this.variant,
                persist: this.persist,
                anchorOrigin: {
                    vertical: this.vertical,
                    horizontal: this.horizontal,
                },
                action: action
            }
        };
    }

    createSnackbarActions(withCloseAction: boolean, actions: Array<IAlertAction>): (key: string) => React.ReactNode {
        return (key: string) => <SnackbarActions snackbarKey={key} withClose={withCloseAction} actions={actions}/>;
    }
}

class AlertUtils {
    success(msg: string): IAlertModel {
        return AlertUtils._withVariant(msg, 'success');
    }
    info(msg: string): IAlertModel {
        return AlertUtils._withVariant(msg, 'info');
    }
    warning(msg: string): IAlertModel {
        return AlertUtils._withVariant(msg, 'warning');
    }
    error(msg: string): IAlertModel {
        return AlertUtils._withVariant(msg, 'error');
    }
    regular(msg: string): IAlertModel {
        return AlertUtils._withVariant(msg, 'default');
    }
    private static _withVariant(msg: string, variant: VariantType): IAlertModel {
        return new AlertBuilder(msg).withVariant(variant).build();
    }
}

export const Alerts: AlertUtils = new AlertUtils();