import React, {useEffect, useState} from 'react';
import {DIALOG_EVENT, EventRegister} from "../app/EventBus";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import {DialogAlertBuilder, IDialogAction, IDialogAlertModel} from "./DialogModel";
import {DialogActions} from "@material-ui/core";
import Button from "@material-ui/core/Button";

class DialogAlertsQueue {
    private queue: Array<IDialogAlertModel> = [];
    private blocked: boolean= false;

    push(data: IDialogAlertModel) {
        this.queue.push(data);
    }
    getNext(): IDialogAlertModel | undefined {
        return this.queue.shift();
    }
    isEmpty(): boolean {
        return this.queue.length === 0;
    }

    isBlocked(): boolean {
        return this.blocked;
    }
    setBlocked(open: boolean) {
        this.blocked = open;
    }
}
const DialogQueue = new DialogAlertsQueue();

export const DialogAlerts: React.FC = () => {
    const [open, setOpen] = useState(false);
    const [state, setState] = useState<IDialogAlertModel>(DialogAlertBuilder.empty());

    const onClose = () => {
        setOpen(false);
        DialogQueue.setBlocked(false);
        nextState();
    };

    const onAction = (action: IDialogAction["callback"]) => () => {
        onClose();
        action();
    };

    const onCancel = () => {
        if (state.withCancel) onClose();
    };

    const onAlert = (data: IDialogAlertModel) => {
        DialogQueue.push(data);
        nextState();
    };

    const nextState = () => {
        if (DialogQueue.isBlocked() || DialogQueue.isEmpty()) {
            return;
        }
        const next = DialogQueue.getNext();
        if (next) {
            DialogQueue.setBlocked(true);
            setState(next);
            setOpen(true);
        }
    };

    useEffect(() => {
        const listener = EventRegister.subscribe(DIALOG_EVENT, onAlert);
        return () => EventRegister.unsubscribe(listener);
    });

    return(
        <Dialog
            fullWidth
            maxWidth='sm'
            open={open}
            onClose={onCancel}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-content"
        >
            <DialogTitle id="alert-dialog-title">{state.title}</DialogTitle>
            <DialogContent dividers={Boolean(state.customContent)}>
                {state.customContent}
                {state.contentText && <DialogContentText id="alert-dialog-content">{state.contentText}</DialogContentText>}
            </DialogContent>
            <DialogActions>
                {state.withCancel &&
                    <Button
                        onClick={onCancel}
                        color='primary'
                        aria-label='Cancel and close popup'
                        title='Cancel and close popup'
                    >
                        Cancel
                    </Button>
                }
                {state.actions.map(action => (
                    <Button
                        onClick={onAction(action.callback)}
                        color='primary'
                        key={action.name}
                        aria-label={action.name}
                    >
                        {action.name}
                    </Button>
                ))}
            </DialogActions>
        </Dialog>
    );
};
