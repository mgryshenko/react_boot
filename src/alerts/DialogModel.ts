import React from "react";

export interface IDialogAction {
    name: string,
    callback: () => void;
}

export interface IDialogAlertModel {
    readonly title: string;
    readonly contentText?: string | null;
    readonly customContent?: React.Component | null;
    readonly actions: Array<IDialogAction>;
    readonly withCancel: boolean;
}

export class DialogAlertBuilder {
    private title: string = '';
    private contentText: string | null = null;
    private customContent: React.Component | null = null;
    private withCancel: boolean = true;
    private actions: Array<IDialogAction> = [];

    withTitle(title: string) {
        this.title = title;
        return this;
    }
    withContentText(text: string | null) {
        this.contentText = text;
        return this;
    }
    withCustomContent(customContent: React.Component | null) {
        this.customContent = customContent;
        return this;
    }
    withAction(name: string, callback: () => void) {
        this.actions.push({name, callback});
        return this;
    }
    withoutCancel() {
        this.withCancel = false;
        return this;
    }

    build(): IDialogAlertModel {
        return {
            title: this.title,
            contentText: this.contentText,
            customContent: this.customContent,
            actions: this.actions,
            withCancel: this.withCancel,
        };
    }

    static empty(): IDialogAlertModel {
        return {
            title: '',
            contentText: null,
            customContent: null,
            actions: [],
            withCancel: true
        };
    }
}
