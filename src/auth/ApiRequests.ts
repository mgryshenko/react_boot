import {authStorage} from "./Auth";
import {API_REFRESH_TOKEN} from "./ApiTemplates";

interface IHeaders {
    [index: string]: string;
}

interface IOptions {
    url: string;
    method: 'POST' | 'GET' | 'DELETE';
    headers: IHeaders;
    body?: string | undefined;
}

class OptionsBuilder {
    createOptions(url: IOptions['url'], method: IOptions['method'], body?: object): IOptions {
        return {
            headers: {
                'Content-Type': 'application/json',
            },
            url: url,
            method: method,
            body: body ? JSON.stringify(body) : undefined,
        }
    }
    createOptionsGet(url: IOptions['url']): IOptions {
        return this.createOptions(url, 'GET');
    }
    createOptionsDelete(url: IOptions['url']): IOptions {
        return this.createOptions(url, 'DELETE');
    }
    createOptionsPost(url: IOptions['url'], body?: object): IOptions {
        return this.createOptions(url, 'POST', body);
    }
    setHeader(options: IOptions, name: string, value: string): void {
        options.headers[name] = value;
    }
}
export const RequestOptions = new OptionsBuilder();

interface IApiRequests {
    requestGet<T>(url: IOptions['url'], requireAuth?: boolean): Promise<T>;
    requestPost<T>(url: IOptions['url'], data: object, requireAuth?: boolean): Promise<T>;
    requestDelete<T>(url: IOptions['url'], requireAuth?: boolean): Promise<T>;
    request<T>(opts: IOptions): Promise<T>;
    userRequest<T>(opts: IOptions): Promise<T>;
    updateRefreshToken<T>(token: string): Promise<void>;
}
class ApiRequests implements IApiRequests {
    requestGet<T>(url: IOptions['url'], requireAuth?: boolean): Promise<T> {
        return this.resolveAuth(RequestOptions.createOptionsGet(url), requireAuth);
    }
    requestPost<T>(url: IOptions['url'], data: object, requireAuth?: boolean): Promise<T> {
        return this.resolveAuth(RequestOptions.createOptionsPost(url, data), requireAuth);
    }
    requestDelete<T>(url: IOptions['url'], requireAuth?: boolean): Promise<T> {
        return this.resolveAuth(RequestOptions.createOptionsDelete(url), requireAuth);
    }

    resolveAuth<T>(opts: IOptions, requireAuth?: boolean): Promise<T> {
        return requireAuth ? this.userRequest(opts) : this.request(opts);
    }

    async request<T>(opts: IOptions): Promise<T> {
        let response = await fetch(opts.url, opts);
        if (response.status === 204 || response.status === 202) {
            return new Promise(resolve => resolve()); //empty body response
        }
        let json = await response.json();
        if (!response.ok) return Promise.reject(json);
        return json;
    }

    userRequest<T>(opts: IOptions): Promise<T> {
        const access = authStorage.getAccessToken();
        const refresh = authStorage.getRefreshToken();
        if (access) return this.withAccessToken(opts, access);
        if (refresh) return this.withRefreshToken(opts, refresh);
        return Promise.reject("No authentication tokens to request resource: " + opts.url);
    }
    withAccessToken<T>(opts: IOptions, token: string): Promise<T> {
        RequestOptions.setHeader(opts, 'Authorization', 'Bearer ' + token);
        return this.request(opts);
    }
    withRefreshToken<T>(opts: IOptions, token: string): Promise<T> {
        return this.updateRefreshToken(token)
            .then(() => {
                const access = authStorage.getAccessToken();
                if (access) return this.withAccessToken(opts, access);
                return Promise.reject("Failed to update authentication tokens. Try again later, or re-login")
            });
    }
    updateRefreshToken(token: string): Promise<void> {
        return this.request(RequestOptions.createOptionsPost(API_REFRESH_TOKEN, {refreshToken : token}))
            .then((response: any) => authStorage.setAuthTokens(response))
            .catch(error => {
                authStorage.setAuthTokens();
                return Promise.reject(error);
            });
    }
}
export const RequestHandlers: IApiRequests = new ApiRequests();