import {Templates} from "./ApiTemplates";
import {EventRegister, REFRESH_TOKEN_EVENT} from "../app/EventBus";
import {RequestHandlers} from "./ApiRequests";

export const LS_AUTH_TOKEN = 'authToken';
export const LS_CURRENT_USER = 'currentUser';

export interface IUserRoles {
    [index: string]: string;
}

export interface IAuthUser {
    id?: number;
    email?: string;
    name?: string;
    imageUrl?: string;
    enabled?: boolean;
    roles?: Array<IUserRoles>,
}

export interface IAuthRaw {
    token?: string | undefined;
    email?: string | undefined;
    iat?: number | undefined;
    exp?: number | undefined;
    refresh_token?: string | undefined;
    refresh_exp?: number | undefined;
}

interface IAuth extends IAuthRaw {
    refresh_token: string;
    refresh_exp: number;
}

export class AuthStorage {
    EXPIRE_THRESHOLD_MS: number = 500;
    scheduledRefreshTask: NodeJS.Timeout | null = null;
    auth: IAuth | null = null;
    user: IAuthUser | null = null;

    constructor() {
        this.subscribeToRefreshEvents();
        this.setAuthTokens(AuthStorage.jsonParseNullable(localStorage.getItem(LS_AUTH_TOKEN)));
        this.setAuthUser(AuthStorage.jsonParseNullable(localStorage.getItem(LS_CURRENT_USER)));
    }

    subscribeToRefreshEvents() {
        EventRegister.subscribe(REFRESH_TOKEN_EVENT, () => {
            const token = this.getRefreshToken();
            if (token) {
                RequestHandlers.updateRefreshToken(token);
            }
        })
    }

    scheduleRefreshToken() {
        AuthStorage.clearTimeoutNullable(this.scheduledRefreshTask);
        if (this.auth && this.auth.refresh_exp) {
            this.scheduledRefreshTask = setTimeout(() => {
                EventRegister.dispatch(REFRESH_TOKEN_EVENT);
            }, this.auth.refresh_exp - Date.now());
        }
    }

    /**
     * @param tokens {
     *   "token": "secret",
     *   "email": "user@user.com",
     *   "iat": 1571129149,
     *   "exp": 1571215549,
     *   "refresh_token": "secret",
     *   "refresh_exp": 1571733949
     * }
     */
    setAuthTokens(tokens?: IAuthRaw) {
        const auth = AuthStorage.parseAuthToken(tokens);
        if (auth) {
            this.applyAuth(auth);
        } else {
            this.clearAuth();
        }
    };

    setAuthUser(user?: IAuthUser) {
        if (user) {
            this.user = user;
            localStorage.setItem(LS_CURRENT_USER, JSON.stringify(user));
        } else {
            this.user = null;
            localStorage.removeItem(LS_CURRENT_USER);
        }
    };

    getAuthUser() {
        return this.user;
    }

    getAccessToken() {
        if (!this.auth || !this.auth.exp) return null;
        return this.isExpired(this.auth.exp) ? null : this.auth.token;
    }

    getRefreshToken(): string | null {
        if (!this.auth || !this.auth.refresh_exp) return null;
        return this.isExpired(this.auth.refresh_exp) ? null : this.auth.refresh_token;
    }

    resetWithRefreshToken = (token: string) => {
        const minuteInMillis = 1000 * 60;
        this.applyAuth({
            refresh_token: token,
            refresh_exp: Date.now() + minuteInMillis,
        })
    };

    private applyAuth(auth: IAuth) {
        const timeout = auth.refresh_exp - Date.now() - this.EXPIRE_THRESHOLD_MS;
        if (timeout < 0) {
            this.clearAuth();
            return;
        }
        this.auth = auth;
        localStorage.setItem(LS_AUTH_TOKEN, JSON.stringify(auth));
        this.scheduleRefreshToken();
    }

    private clearAuth() {
        this.auth = null;
        AuthStorage.clearTimeoutNullable(this.scheduledRefreshTask);
        localStorage.removeItem(LS_AUTH_TOKEN);
    }

    private isExpired(exp: number): boolean {
        return Date.now() > (exp - this.EXPIRE_THRESHOLD_MS);
    }

    private static parseAuthToken(tokens?: IAuthRaw): IAuth | null {
        if (!tokens || !tokens.refresh_token || !tokens.refresh_exp) {
            return null;
        }
        return {
            token: tokens.token,
            email: tokens.email,
            iat: AuthStorage.parseMillisISO8601(tokens.iat),
            exp: AuthStorage.parseMillisISO8601(tokens.exp),
            refresh_token: tokens.refresh_token,
            refresh_exp: AuthStorage.parseMillisISO8601(tokens.refresh_exp),
        };
    }

    private static parseMillisISO8601(millis: number | undefined): number {
        if (!millis) return 0;
        let millisString = millis.toString();
        const missedZeros = millisString.length;
        for (let i = 0; i < (13 - missedZeros); i++) {
            millisString += '0';
        }
        return parseInt(millisString);
    }

    private static clearTimeoutNullable(timeout: NodeJS.Timeout | null) {
        if (timeout) { clearTimeout(timeout) }
    }

    private static jsonParseNullable<T>(text: string | null): T | undefined {
        return text ? JSON.parse(text) : undefined;
    }
}

interface ILoadUserCallback {
    (user: IAuthUser | null): void;
}
export class UserLoader {
    authStorage: AuthStorage;
    userLoading: boolean = false;
    onLoadCallbacks: Array<ILoadUserCallback> = [];

    constructor(authStorage: AuthStorage) {
        this.authStorage = authStorage;
    }

    load(callback: ILoadUserCallback) {
        this.onLoadCallbacks.push(callback);
        if (!this.isLoading()) {
            this.userLoading = true;
            this.startLoading(user => this.onLoaded(user));
        }
    }

    private isLoading(): boolean {
        return this.userLoading;
    }

    private startLoading(callback: ILoadUserCallback) {
        Templates.userMe()
            .then(user => {
                authStorage.setAuthUser(user);
                callback(user);
            })
            .catch(() => callback(null))
    }

    private onLoaded(user: IAuthUser | null) {
        this.userLoading = false;
        const callbacks = this.onLoadCallbacks.splice(0, this.onLoadCallbacks.length);
        callbacks.forEach(onLoad => onLoad(user));
    }
}

export const authStorage = new AuthStorage();
export const userLoader = new UserLoader(authStorage);

export function login(form: {email: string, password: string}): Promise<void> {
    return Templates.login(form)
        .then((response: any) => {
            console.log('[Auth][login] success');
            authStorage.setAuthTokens(response);
        })
}

export function logout() {
    console.log('[Auth][logout]');
    authStorage.setAuthTokens();
    authStorage.setAuthUser();
}