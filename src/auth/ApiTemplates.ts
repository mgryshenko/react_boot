import {RequestHandlers} from "./ApiRequests";
import {IAuthRaw, IAuthUser} from "./Auth";

export const API_BASE_URL = 'https://localhost:8443/api';
export const API_LOGIN = API_BASE_URL + '/auth/login';
export const API_REGISTER = API_BASE_URL + '/auth/register';
export const API_RESET_PASSWORD = API_BASE_URL + '/auth/password/reset';
export const API_CHANGE_PASSWORD = API_BASE_URL + '/auth/password/change';
export const API_SAVE_PASSWORD = API_BASE_URL + '/auth/password/save';
export const API_REFRESH_TOKEN = API_BASE_URL + '/auth/token/refresh';

export const API_RESOURCE_URL = API_BASE_URL + '';
export const API_USER_ME = API_RESOURCE_URL + '/user/me';

export const OAUTH2_LOGIN_REDIRECT_URI = 'https://' + window.location.host + '/oauth2/login/redirect';
export const API_GOOGLE_REGISTER_URL = API_BASE_URL + '/oauth2/register/google?redirect_uri=' + OAUTH2_LOGIN_REDIRECT_URI;
export const API_GITHUB_REGISTER_URL = API_BASE_URL + '/oauth2/register/github?redirect_uri=' + OAUTH2_LOGIN_REDIRECT_URI;
export const API_FACEBOOK_REGISTER_URL = API_BASE_URL + '/oauth2/register/facebook?redirect_uri=' + OAUTH2_LOGIN_REDIRECT_URI;
export const API_GOOGLE_AUTH_URL = API_BASE_URL + '/oauth2/authorize/google?redirect_uri=' + OAUTH2_LOGIN_REDIRECT_URI;
export const API_GITHUB_AUTH_URL = API_BASE_URL + '/oauth2/authorize/github?redirect_uri=' + OAUTH2_LOGIN_REDIRECT_URI;
export const API_FACEBOOK_AUTH_URL = API_BASE_URL + '/oauth2/authorize/facebook?redirect_uri=' + OAUTH2_LOGIN_REDIRECT_URI;

interface IFormTemplates {
    login: {
        email: string;
        password: string;
    };
    resetPassword: {
        email: string;
        completeUri: string;
    };
    savePassword: {
        token: string;
        password: string;
        matchingPassword: string;
    };
    changePassword: {
        password: string;
        oldPassword: string;
    };
    register: {
        password: string;
        oldPassword: string;
    };
    registerConfirm: {
        token: string;
    };
}
interface IResponseTemplates {
    registerConfirm: {success: true | false | "true" | "false";};
}

class ApiTemplates {
    deleteMe(): Promise<void> {
        return RequestHandlers.requestDelete<void>(API_USER_ME, true);
    }
    userMe(): Promise<IAuthUser> {
        return RequestHandlers.requestGet<IAuthUser>(API_USER_ME, true);
    }
    login(form: IFormTemplates['login']): Promise<IAuthRaw> {
        return RequestHandlers.requestPost<IAuthRaw>(API_LOGIN, form);
    }
    resetPassword(form: IFormTemplates['resetPassword']): Promise<void> {
        return RequestHandlers.requestPost<void>(API_RESET_PASSWORD, form);
    }
    savePassword(form: IFormTemplates['savePassword']): Promise<void> {
        return RequestHandlers.requestPost<void>(API_SAVE_PASSWORD, form);
    }
    changePassword(form: IFormTemplates['changePassword']): Promise<void> {
        return RequestHandlers.requestPost<void>(API_CHANGE_PASSWORD, form, true);
    }
    register(form: IFormTemplates['register']): Promise<void> {
        return RequestHandlers.requestPost(API_REGISTER, form);
    }
    registerConfirm(token: IFormTemplates['registerConfirm']): Promise<IResponseTemplates['registerConfirm']> {
        return RequestHandlers.requestGet<IResponseTemplates['registerConfirm']>(API_REGISTER + "/?token=" + token);
    }
}

export const Templates = new ApiTemplates();
