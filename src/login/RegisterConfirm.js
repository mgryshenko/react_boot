import React, {useEffect, useState} from 'react';
import {PATH_LOGIN, parseUrlParameter} from "../app/AppRoutes";
import {Link} from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core";
import Fade from "@material-ui/core/Fade";
import CircularProgress from "@material-ui/core/CircularProgress";
import {Templates} from "../auth/ApiTemplates";
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import Paper from "@material-ui/core/Paper";

const ERROR_REGISTER_CONFIRM_FAILED_MSG = "Broken confirmation token. Check if confirmation link sent to your mailbox not expired or broken";

const useStyles = makeStyles(theme => ({
    icon: {
        width: 60,
        height: 60,
    },
    margin: {
        marginTop: theme.spacing(3),
    },
    paper: {
        marginTop: theme.spacing(8),
        padding: theme.spacing(3, 2),
        alignItems: 'center',
    },
    error: {
        color: 'red',
    },
}));

function useConfirmRegisterState(token) {
    const [isStarted, setIsStarted] = useState(false);
    const [state, setState] = useState({
        loaded: false,
        success: false,
        error: ''
    });

    const isSucceed = result => {
        return result.success && result.success !== "false"
    };

    useEffect(() => {
        if (isStarted) return;
        setIsStarted(true);

        const mergeResult = result => setState(Object.assign({}, state, result));
        const onError = msg =>  mergeResult({loaded: true, error: msg});
        const onSuccess = () => mergeResult({loaded: true, success:true});

        Templates.registerConfirm(token)
            .then(result => isSucceed(result) ? onSuccess() : onError(ERROR_REGISTER_CONFIRM_FAILED_MSG))
            .catch(error => onError(error.error.message));
    }, [state, token, isStarted]);

    return state;
}

export default function RegisterConfirm(props) {
    const classes = useStyles();

    const token = parseUrlParameter('token', props);
    const state = useConfirmRegisterState(token);

    return (
        <Container maxWidth='sm' className={classes.margin}>
            <Paper className={classes.paper}>

                {!state.loaded &&
                    <Container align='center'>
                        <Typography component="h3" variant="h5" align="center">
                            Confirming registration...
                        </Typography>
                        <Fade in={!state.loaded} style={{ transitionDelay: state.loaded ? '0ms' : '800ms', }} unmountOnExit>
                            <CircularProgress />
                        </Fade>
                    </Container>
                }
                {state.loaded &&
                    <React.Fragment>
                        <Container align='center'>
                            {state.success &&<CheckCircleOutlineIcon className={classes.icon} color='primary'/>}
                            {!state.success &&<HighlightOffIcon className={classes.icon} color='error'/>}
                        </Container>
                        <Container maxWidth="sm" className={classes.margin}>
                            {state.success &&
                            <Button component={ Link } to={PATH_LOGIN} align='center' size="large" aria-label="Sign in" title="Sign in">
                                Sign in
                            </Button>
                            }
                            {!state.success &&
                                <Typography component="h3" variant="h5">Registration not confirmed</Typography>
                            }
                            {!state.success &&
                                <Typography component="p" >{state.error}</Typography>
                            }
                        </Container>
                    </React.Fragment>
                }
            </Paper>
        </Container>

    );
}