import React, {useState} from 'react';
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import {parseUrlParameter, PATH_LOGIN} from "../app/AppRoutes";
import {makeStyles} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import FormField, {FormPasswordField, Validators} from "./FormField";
import {Templates} from "../auth/ApiTemplates";
import {ALERT_EVENT, EventRegister} from "../app/EventBus";
import {Alerts} from "../alerts/AlertModel";

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        padding: theme.spacing(3, 2),
        alignItems: 'center',
    },
    divider: {
        width: '100%', // Fix IE 11 issue.
        padding: '1px',
        marginBottom: theme.spacing(2),
    },
    form: {
        width: '100%', // Fix IE 11 issue.
    },
    hidden: {
        display: 'none',
    },
    submit: {
        textTransform: 'none',
        marginTop: theme.spacing(1),
    },
    error: {
        color: 'red',
    },
}));

function PasswordResetConfirmForm(props) {
    const classes = useStyles();
    const model = {
        password: {id: 'password', validator: value => Validators.validatePassword(value)},
        matchingPassword: {id: 'matchingPassword', validator: value => Validators.validatePassword(value)},
    };
    const [form, setForm] = useState({
        token: props.secret,
    });
    const [error, setError] = useState(null);

    const onInputChange = input => e => {
        setForm(Object.assign({}, form, {[input]: e.target.value}));
    };

    const validateFields = () => {
        let valid = true;
        Object.keys(model).forEach(key => {valid = valid && model[key].validator(form[model[key].id]).success});
        setError(valid ? null : {message: 'Passwords should match and be valid'});
        if (valid && (form[model.password.id] !== form[model.matchingPassword.id])) {
            valid = false;
            setError({message: "Repeated password doesn't match to new password"});
        }
        return valid;
    };

    const onSubmit = (e) => {
        e.preventDefault();
        if (validateFields()) props.onSubmit(form);
    };

    return (
        <form className={classes.form} onSubmit={onSubmit} noValidate>
            {error &&
                <Typography className={classes.error}>{error.message}</Typography>
            }
            {!error && props.error &&
                <Typography className={classes.error}>{props.error.message}</Typography>
            }
            <FormField className={classes.hidden} id='email' autoComplete="email" disabled />
            <FormPasswordField
                id={model.password.id}
                autoComplete="new-password"
                label="New password"
                error={error && !form[model.password.id]}
                validator={model.password.validator}
                onChange={onInputChange(model.password.id)}
            />
            <FormPasswordField
                id={model.matchingPassword.id}
                autoComplete="new-password"
                label="Repeat your new password"
                error={error && !form[model.matchingPassword.id]}
                validator={model.matchingPassword.validator}
                onChange={onInputChange(model.matchingPassword.id)}
            />
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                size='large'
                disabled={props.disabled}
            >
                Save new password
            </Button>
        </form>
    );
}


export default function PasswordResetConfirm(props) {
    const classes = useStyles();
    const token = parseUrlParameter('token', props);

    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);

    const handleSubmit = function(form) {
        setLoading(true);
        Templates.savePassword(form)
            .then(() => {
                EventRegister.dispatch(ALERT_EVENT, Alerts.success("Your password changed"));
                props.history.push(PATH_LOGIN);
            })
            .catch((response) => {
                setLoading(false);
                setError(response.error);
            })
    };

    return (
        <Container component="main" maxWidth="xs">
            <Paper className={classes.paper} elevation={6}>
                <Typography component="h1" variant="h5" align='center'>
                    Type your new password
                </Typography>
                <PasswordResetConfirmForm
                    secret={token}
                    error={error}
                    disabled={loading}
                    onSubmit={handleSubmit}
                />
            </Paper>
        </Container>
    );
}