import React, {useState} from 'react';
import Container from "@material-ui/core/Container";
import {makeStyles} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import FormField from "./FormField";
import Button from "@material-ui/core/Button";
import {PATH_CONFIRM_RESET_PASSWORD, PATH_LOGIN} from "../app/AppRoutes";
import {Templates} from "../auth/ApiTemplates";
import {ALERT_EVENT, EventRegister} from "../app/EventBus";
import {Alerts} from "../alerts/AlertModel";

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        padding: theme.spacing(3, 2),
        alignItems: 'center',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
    },
    submit: {
        textTransform: 'none',
        marginTop: theme.spacing(1),
    },
    error: {
        color: 'red',
    },
}));

function PasswordResetForm(props) {
    const classes = useStyles();
    const model = {email: 'email'};
    const [form, setForm] = useState({
        completeUri: PATH_CONFIRM_RESET_PASSWORD,
    });
    const [error, setError] = useState(null);

    const onInputChange = input => e => {
        setForm(Object.assign({}, form, {[input]: e.target.value}));
    };

    const validateFields = () => {
        let filled = true;
        Object.keys(model).forEach(key => {filled = filled && Boolean(form[model[key]])});
        setError(filled ? null : {message: "Email must be filled"});
        return filled;
    };

    const onSubmit = (e) => {
        e.preventDefault();
        if (validateFields()) props.onSubmit(form);
    };

    return (
        <form className={classes.form} noValidate onSubmit={onSubmit}>
            {error &&
                <Typography className={classes.error}>{error.message}</Typography>
            }
            {!error && props.error &&
                <Typography className={classes.error}>Login failed. {props.error.message}</Typography>
            }
            <FormField
                id={model.email}
                label="Email Address"
                autoComplete="email"
                type='email'
                autoFocus
                error={error && !form[model.email]}
                onChange={onInputChange(model.email)}
            />
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                size='large'
                disabled={props.loading}
            >
                Send verification link
            </Button>
        </form>
    );
}

export default function PasswordReset(props) {
    const classes = useStyles();

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const handleSubmit = function(form) {
        setLoading(true);
        Templates.resetPassword(form)
            .then(() => {
                EventRegister.dispatch(ALERT_EVENT, Alerts.success("Check your mailbox to complete password resetting"));
                props.history.push(PATH_LOGIN);
            })
            .catch(response => {
                setLoading(false);
                setError(response.error)
            });
    };

    return (
        <Container maxWidth="xs">
            <Paper className={classes.paper} elevation={6} >
                <Typography component="h1" variant="h5" align='center' >
                    Sign in
                </Typography>
                <PasswordResetForm error={error} loading={loading} onSubmit={handleSubmit} />
            </Paper>
        </Container>
    );
}