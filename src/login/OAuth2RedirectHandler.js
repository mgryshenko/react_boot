import React from 'react';
import {authStorage} from "../auth/Auth";
import {PATH_HOME, PATH_LOGIN, parseUrlParameter} from "../app/AppRoutes";
import {Redirect} from "react-router-dom";
import {ALERT_EVENT, EventRegister} from "../app/EventBus";
import {Alerts} from "../alerts/AlertModel";

export default function OAuth2RedirectHandler(props) {
    const token = parseUrlParameter('token', props);
    const error = parseUrlParameter('error', props);

    if (token) {
        authStorage.resetWithRefreshToken(token);
        return <Redirect to={{ pathname: PATH_HOME, state: {from: props.location} }} />;
    } else {
        console.log('[OAuth2RedirectHandler] error: ' + error);
        new Promise(resolve => setTimeout(() => resolve(), 800))
            .then(() => EventRegister.dispatch(ALERT_EVENT, Alerts.error(error)));
        return <Redirect to={{ pathname: PATH_LOGIN, state: {from: props.location} }} />;
    }
}