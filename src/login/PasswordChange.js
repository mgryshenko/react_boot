import React, {useState} from 'react';
import {Templates} from "../auth/ApiTemplates";
import {ALERT_EVENT, EventRegister} from "../app/EventBus";
import {Alerts} from "../alerts/AlertModel";
import {PATH_PROFILE} from "../app/AppRoutes";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core";
import {FormPasswordField, Validators} from "./FormField";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        padding: theme.spacing(3, 2),
        alignItems: 'center',
    },
    divider: {
        width: '100%', // Fix IE 11 issue.
        padding: '1px',
        marginBottom: theme.spacing(2),
    },
    form: {
        width: '100%', // Fix IE 11 issue.
    },
    submit: {
        textTransform: 'none',
        marginTop: theme.spacing(1),
    },
    error: {
        color: 'red',
    },
    noMargin: {
        marginTop: 0,
    },
}));

function PasswordChangeForm(props) {
    const classes = useStyles();
    const model = {
        oldPassword: {id: 'oldPassword', validator: () => Validators.success()},
        password: {id: 'password', validator: value => Validators.validatePassword(value)},
        matchingPassword: {id: 'matchingPassword', validator: value => Validators.validatePassword(value)},
    };
    const [form, setForm] = useState({});
    const [error, setError] = useState(null);

    const onInputChange = input => e => {
        setForm(Object.assign({}, form, {[input]: e.target.value}));
    };

    const validateFields = () => {
        let valid = true;
        Object.keys(model).forEach(key => {valid = valid && model[key].validator(form[model[key].id]).success});
        setError(valid ? null : {message: 'All fields should be filled and valid'});
        if (valid && (form[model.password.id] !== form[model.matchingPassword.id])) {
            valid = false;
            setError({message: "Repeated password doesn't match to new password"});
        }
        return valid;
    };

    const onSubmit = (e) => {
        e.preventDefault();
        if (validateFields()) {
            delete form[model.matchingPassword.id];
            props.onSubmit(form);
        }
    };

    return (
        <form className={classes.form} onSubmit={onSubmit} noValidate>
            {error &&
                <Typography className={classes.error}>{error.message}</Typography>
            }
            {!error && props.error &&
                <Typography className={classes.error}>{props.error.message}</Typography>
            }
            <Typography variant="subtitle1" align='left' >
                Type your current password:
            </Typography>
            <FormPasswordField
                className={classes.noMargin}
                id={model.oldPassword.id}
                autoComplete="current-password"
                label="Current password"
                error={error && !form[model.oldPassword.id]}
                validator={model.oldPassword.validator}
                onChange={onInputChange(model.oldPassword.id)}
            />
            <Typography variant="subtitle1" align='left' >
                Create your new password:
            </Typography>
            <FormPasswordField
                className={classes.noMargin}
                id={model.password.id}
                label="Enter your new password"
                error={error && !form[model.password.id]}
                validator={model.password.validator}
                onChange={onInputChange(model.password.id)}
            />
            <FormPasswordField
                id={model.matchingPassword.id}
                label="Repeat your new password"
                error={error && !form[model.matchingPassword.id]}
                validator={model.matchingPassword.validator}
                onChange={onInputChange(model.matchingPassword.id)}
            />
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                size='large'
                disabled={props.disabled}
            >
                Save new password
            </Button>
        </form>
    );
}

export default function PasswordChange(props) {
    const classes = useStyles();

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const handleSubmit = function(form) {
        setLoading(true);
        Templates.changePassword(form)
            .then(() => {
                EventRegister.dispatch(ALERT_EVENT, Alerts.success("Your password changed"));
                props.history.push(PATH_PROFILE);
            })
            .catch(response => {
                setLoading(false);
                setError(response.error)
            });
    };

    return (
        <Container maxWidth="xs">
            <Paper className={classes.paper} elevation={6} >
                <Typography component="h1" variant="h5" align='center' >
                    Change password
                </Typography>
                <PasswordChangeForm error={error} loading={loading} onSubmit={handleSubmit} />
            </Paper>
        </Container>
    );
}