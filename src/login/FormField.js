import React, {useState} from "react";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import {Visibility, VisibilityOff} from "@material-ui/icons";

export const Validators = {
    validateName: function (name) {
        if (name && name.length > 0) return this.success();
        return this.error("Name shouldn't be empty");
    },
    validateEmail: function (email) {
        const re = /^(([a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?))$/;
        if (re.test(String(email).toLowerCase())) return this.success();
        return this.error('Email must look like "your.any.name@company.domain"');
    },
    validatePassword: function (pwd) {
        if (pwd && pwd.length >= 8) return this.success();
        return this.error('Password must be at least 8 characters long');
    },
    success: function() {return this.createResult(true, '')},
    error: function(msg) {return this.createResult(false, msg)},
    createResult: function(success, errorMessage) {return {success: success, error: errorMessage}}
};

export default function FormField(props) {
    return (
        <TextField
            name={props.id}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            disabled={props.disabled}
            {...props}
        />
    );
}

export function ValidationField({onChange, error, helperText, validator, ...props}) {
    const [state, setState] = useState({success: true, error: ''});
    const validate = validator ? validator : () => Validators.success();

    const onInputChange = event => {
        const nextState = validate(event.target.value);
        setState(nextState);
        onChange(event);
    };

    return (
        <FormField
            onChange={onInputChange}
            error={!state.success || error}
            helperText={!state.success ? state.error : helperText}
            {...props}
        />
    );
}

export function FormPasswordField({type, InputProps, ...props}) {
    const [showPassword, setShowPassword] = useState(false);
    return (
        <ValidationField
            type={showPassword ? "default" : "password"}
            InputProps={Object.assign({}, InputProps, {
                endAdornment: (
                    <InputAdornment position="end">
                        <IconButton
                            edge="end"
                            aria-label="toggle password visibility"
                            onClick={() => setShowPassword(!showPassword)}
                        >
                            {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                    </InputAdornment>
                )})
            }
            {...props}
        />
    );
}
