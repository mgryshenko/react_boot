import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {Link} from "react-router-dom";
import Paper from "@material-ui/core/Paper";
import {login} from "../auth/Auth";
import SSOButton, {createProvider} from "./SSOButton";
import {API_FACEBOOK_AUTH_URL, API_GITHUB_AUTH_URL, API_GOOGLE_AUTH_URL} from "../auth/ApiTemplates";
import Divider from "@material-ui/core/Divider";
import GoogleLogo from '../img/logo_google.png';
import GithubLogo from '../img/logo_github.png';
import FacebookLogo from '../img/logo_fb.png';
import ExpansionFormPanel from "./ExpansionFormPanel";
import FormField, {FormPasswordField} from "./FormField";
import {PATH_HOME, PATH_REGISTER, PATH_PASSWORD_RESET} from "../app/AppRoutes";

const providers = [
    createProvider('google',API_GOOGLE_AUTH_URL,GoogleLogo),
    createProvider('facebook',API_FACEBOOK_AUTH_URL,FacebookLogo),
    createProvider('github',API_GITHUB_AUTH_URL,GithubLogo),
];

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        padding: theme.spacing(3, 2),
        alignItems: 'center',
    },
    divider: {
        width: '100%', // Fix IE 11 issue.
        padding: '1px',
        marginBottom: theme.spacing(2),
    },
    form: {
        width: '100%', // Fix IE 11 issue.
    },
    submit: {
        textTransform: 'none',
        marginTop: theme.spacing(1),
    },
    error: {
        color: 'red',
    },
}));

function SignInForm(props) {
    const classes = useStyles();
    const model = {email: 'email', password: 'password'};
    const [form, setForm] = useState({});
    const [error, setError] = useState(null);

    const onInputChange = input => e => {
        setForm(Object.assign({}, form, {[input]: e.target.value}));
    };

    const validateFields = () => {
        let filled = true;
        Object.keys(model).forEach(key => {filled = filled && Boolean(form[model[key]])});
        setError(filled ? null : {message: 'All required fields must be filled'});
        return filled;
    };

    const onSubmit = (e) => {
        e.preventDefault();
        if (validateFields()) props.onSubmit(form);
    };
    return (
        <form className={classes.form} noValidate onSubmit={onSubmit}>
            {error &&
                <Typography className={classes.error}>{error.message}</Typography>
            }
            {!error && props.error &&
                <Typography className={classes.error}>Login failed. {props.error.message}</Typography>
            }
            <FormField
                id={model.email}
                label="Email Address"
                autoComplete="email"
                type="email"
                autoFocus
                error={error && !form[model.email]}
                onChange={onInputChange(model.email)}
            />
            <FormPasswordField
                id={model.password}
                label="Password"
                autoComplete="current-password"
                error={error && !form[model.password]}
                onChange={onInputChange(model.password)}
            />
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                size='large'
                disabled={props.loading}
            >
                Sign in
            </Button>
        </form>
    );
}

export default function SignIn(props) {
    const classes = useStyles();

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const handleSubmit = function(form) {
        setLoading(true);
        login(form)
            .then(() => {
                props.onLogin();
                props.history.push(PATH_HOME);
            })
            .catch(response => {
                setLoading(false);
                setError(response.error)
            });
    };

    return (
        <Container maxWidth="xs">
            <Paper className={classes.paper} elevation={6} >
                <Typography component="h1" variant="h5" align='center' >
                    Sign in
                </Typography>
                {providers.map(provider => (
                    <SSOButton key={provider.name} disabled={loading} provider={provider} title={"Sign in with " + provider.name} />
                ))}
                <ExpansionFormPanel
                    title='Sign in without social profile'
                    component={<SignInForm error={error} loading={loading} onSubmit={handleSubmit}/>}
                />
                <Divider className={classes.divider} />
                <Grid container>
                    <Grid item xs>
                        <Link to={PATH_PASSWORD_RESET} variant="body2">
                            Forgot password?
                        </Link>
                    </Grid>
                    <Grid item>
                        <Link to={PATH_REGISTER} variant="body2">
                            Don't have an account? Sign Up
                        </Link>
                    </Grid>
                </Grid>
            </Paper>
        </Container>
    );
}
