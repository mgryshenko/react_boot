import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {Link} from "react-router-dom";
import Paper from "@material-ui/core/Paper";
import {
    API_GOOGLE_REGISTER_URL,
    API_FACEBOOK_REGISTER_URL,
    API_GITHUB_REGISTER_URL,
    Templates
} from "../auth/ApiTemplates";
import GoogleLogo from "../img/logo_google.png";
import FacebookLogo from "../img/logo_fb.png";
import GithubLogo from "../img/logo_github.png";
import SSOButton, {createProvider} from "./SSOButton";
import Divider from "@material-ui/core/Divider";
import ExpansionFormPanel from "./ExpansionFormPanel";
import {login} from "../auth/Auth";
import {
    FormPasswordField,
    ValidationField,
    Validators
} from "./FormField";
import {PATH_ABOUT, PATH_CONFIRM_REGISTER, PATH_HOME} from "../app/AppRoutes";
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import {DialogAlertBuilder} from "../alerts/DialogModel";
import {DIALOG_EVENT, EventRegister} from "../app/EventBus";
import Checkbox from "@material-ui/core/Checkbox";
import {FormControlLabel} from "@material-ui/core";

const providers = [
    createProvider('google',API_GOOGLE_REGISTER_URL,GoogleLogo),
    createProvider('facebook',API_FACEBOOK_REGISTER_URL,FacebookLogo),
    createProvider('github',API_GITHUB_REGISTER_URL,GithubLogo),
];

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        padding: theme.spacing(3, 2),
        alignItems: 'center',
    },
    divider: {
        width: '100%', // Fix IE 11 issue.
        padding: '1px',
        marginBottom: theme.spacing(2),
    },
    form: {
        width: '100%', // Fix IE 11 issue.
    },
    submit: {
        textTransform: 'none',
        marginTop: theme.spacing(1),
    },
    error: {
        color: 'red',
    },
    icon: {
        width: 60,
        height: 60,
    },
}));

export const showAgreementDialog = (history, onSubmit) => {
    const content =
        <React.Fragment>
            <Typography gutterBottom>
                In short: do not provide your sensible personal data!
            </Typography>
            <Typography gutterBottom>
                Use existing but fake, anonymous mail accounts, which not linked with your personality. Be ready,
                that your data could be deleted at any time.
            </Typography>
            <Typography gutterBottom>
                This is the pet project, for testing purposes, without any guarantees
            </Typography>
        </React.Fragment>;
    const dialog = new DialogAlertBuilder()
        .withTitle("Signing Up?")
        .withCustomContent(content)
        .withAction("More", () => {
            history.push(PATH_ABOUT);
        })
        .withAction("Agree anyway", onSubmit)
        .build();
    EventRegister.dispatch(DIALOG_EVENT, dialog);
};

function SignUpForm(props) {
    const classes = useStyles();
    const model = {
        name: {id: 'name', validator: value => Validators.validateName(value)},
        email: {id: 'email', validator: value => Validators.validateEmail(value)},
        password: {id: 'password', validator: value => Validators.validatePassword(value)},
    };
    const [form, setForm] = useState({
        completeUri: PATH_CONFIRM_REGISTER,
    });
    const [error, setError] = useState(null);
    const [agree, setAgree] = useState(false);

    const onInputChange = input => e => {
        setForm(Object.assign({}, form, {[input]: e.target.value}));
    };

    const validateFields = () => {
        let valid = true;
        Object.keys(model).forEach(key => {valid = valid && model[key].validator(form[model[key].id]).success});
        setError(valid ? null : {message: 'All required fields must be valid'});
        if (valid && !agree) {
            valid = false;
            setError({message: "You should agree with application rules"});
        }
        return valid;
    };

    const onSubmit = (e) => {
        e.preventDefault();
        if (validateFields()) props.onSubmit(form);
    };

    const onAgree = (e) => {
        if (e.target.checked) {
            showAgreementDialog(props.history, () => setAgree(true));
        } else {
            setAgree(false);
        }
    };

    return (
        <form className={classes.form} onSubmit={onSubmit} noValidate>
            {error &&
                <Typography className={classes.error}>{error.message}</Typography>
            }
            {!error && props.error &&
                <Typography className={classes.error}>{props.error.message}</Typography>
            }
            <ValidationField
                id={model.name.id}
                autoComplete="fname"
                label="First Name"
                autoFocus
                error={error && !form[model.name.id]}
                validator={model.name.validator}
                onChange={onInputChange(model.name.id)}
            />
            <ValidationField
                id={model.email.id}
                autoComplete="email"
                type='email'
                label="Email Address"
                error={error && !form[model.email.id]}
                validator={model.email.validator}
                onChange={onInputChange(model.email.id)}
            />
            <FormPasswordField
                id={model.password.id}
                autoComplete="current-password"
                label="Password"
                error={error && !form[model.password.id]}
                validator={model.password.validator}
                onChange={onInputChange(model.password.id)}
            />
            <FormControlLabel control={
                <Checkbox
                    checked={agree}
                    onChange={onAgree}
                    color='primary'
                    value="Agree with rules"
                    inputProps={{
                        'aria-label': 'Agree with rules',
                    }}
                />
            } label='I agree with rules' />

            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                size='large'
                disabled={props.disabled}
            >
                Sign up
            </Button>
        </form>
    );
}

export default function SignUp(props) {
    const classes = useStyles();

    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);
    const [completed, setCompleted] = useState(false);

    const handleSubmit = form => {
        setLoading(true);
        Templates.register(form)
            .then(() => onRegisterSuccess(form))
            .catch((response) => {
                setLoading(false);
                setError(response.error);
            })
    };

    const onRegisterSuccess = form => {
        login(form)
            .then(() => {
                props.onLogin();
                props.history.push(PATH_HOME);
            })
            .catch(() => setCompleted(true));
    };

    if (completed) {
        return (
            <Container component="main" maxWidth="sm">
                <Paper className={classes.paper} elevation={6}>
                    <Container align='center'>
                        <CheckCircleOutlineIcon className={classes.icon} color='primary'/>
                    </Container>
                    <Container maxWidth="sm" className={classes.margin}>
                        <Typography component="h3" variant="h5">Your registration is almost complete!</Typography>
                        <Typography component="p" >Check your mail box for further instructions</Typography>
                    </Container>
                </Paper>
            </Container>
        );
    }

    const onClick = href => (e) => {
        e.preventDefault();
        showAgreementDialog(props.history, () => {
            window.location.href= href;
        });
    };

    return (
        <Container component="main" maxWidth="xs">
            <Paper className={classes.paper} elevation={6}>
                <Typography component="h1" variant="h5" align='center'>
                    Sign up
                </Typography>
                {providers.map(provider => (
                    <SSOButton key={provider.name}
                               onClick={onClick(provider.url)}
                               disabled={loading}
                               provider={provider}
                               title={"Sign up with " + provider.name}
                    />
                ))}
                <ExpansionFormPanel
                    title='Sign up without social profile'
                    component={<SignUpForm
                        error={error}
                        disabled={loading}
                        onSubmit={handleSubmit}
                        history={props.history}
                    />}
                />
                <Divider className={classes.divider} />
                <Grid container justify='flex-end'>
                    <Grid item>
                        <Typography component={ Link } to="/login" variant="body2">
                            Already have an account? Sign In
                        </Typography>
                    </Grid>
                </Grid>
            </Paper>
        </Container>
    );
}