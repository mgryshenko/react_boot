import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import React from "react";
import {makeStyles} from "@material-ui/core";

export const createProvider = (name, url, logoUrl) => {
    return {
        name: name,
        url: url,
        logoUrl: logoUrl,
    };
};

const useStyles = makeStyles(theme => ({
    button: {
        width: '100%', // Fix IE 11 issue.
        padding: '1px',
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
        minHeight: '46px',
    },
    icon: {
        height: '50%',
        width: '12%',
        padding: '5px',
    },
    text: {
        textTransform: 'none',
        align: 'center',
        width: '80%',
    },
}));

export default function SSOButton(props) {
    const classes = useStyles();

    return (
        <Button
            href={props.provider.url}
            className={classes.button}
            fullWidth
            variant='outlined'
            color="primary"
            size="large"
            aria-label={props.title}
            title={props.title}
            {...props}
        >
            <img className={classes.icon} alt={props.provider.name} src={props.provider.logoUrl}/>
            <Container className={classes.text}>
                <Typography >
                    {props.title}
                </Typography>
            </Container>
        </Button>
    );
}
