import React from 'react';
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import {ExpansionPanel, makeStyles} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    panel: {
        padding: 0,
        marginTop: theme.spacing(1),
    },
    details: {
        padding: 0,
    },
}));

export default function ExpansionFormPanel(props) {
    const classes = useStyles();

    return (
        <ExpansionPanel elevation={0} className={classes.panel}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <Typography>
                    {props.title}
                </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={classes.details}>
                {props.component}
            </ExpansionPanelDetails>
        </ExpansionPanel>
    );
}
