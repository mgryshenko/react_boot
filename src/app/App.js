import React, {useEffect, useState} from 'react';
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import TopBar from './TopBar';
import AppFooter from './Footer';
import {
    Route,
    Switch
} from 'react-router-dom';
import SignIn from "../login/SignIn";
import Home from "../home/Home";
import Projects from "../projects/Projects";
import {makeStyles} from "@material-ui/core";
import LeftDrawer from "./LeftDrawer";
import NotFound from "./NotFound";
import SignUp from "../login/SignUp";
import {logout, userLoader, authStorage} from "../auth/Auth";
import Profile from "../profile/Profile";
import {
    PATH_HOME,
    PATH_LOGIN, PATH_OAUTH2_LOGIN,
    PATH_PROFILE,
    PATH_PROJECTS,
    PATH_REGISTER, PATH_PASSWORD_RESET,
    RedirectRoute, PATH_CONFIRM, PATH_PASSWORD_CHANGE, PATH_ABOUT, PATH_PROJECTS_SVG
} from "./AppRoutes";
import OAuth2RedirectHandler from "../login/OAuth2RedirectHandler";
import SnackbarAlerts from "../alerts/SnackbarAlerts";
import {ALERT_EVENT, EventRegister} from "./EventBus";
import {Alerts} from "../alerts/AlertModel";
import {DialogAlerts} from "../alerts/DialogAlerts";
import PasswordReset from "../login/PasswordReset";
import PasswordResetConfirm from "../login/PasswordResetConfirm";
import PasswordChange from "../login/PasswordChange";
import RegisterConfirm from "../login/RegisterConfirm";
import About from "./About";
import AnimatedSvgProject from "../projects/svg/AnimatedSvgProject";

const useStyles = makeStyles({
    root: {
        flexGrow: 1,
    },
});

export default function App(props) {
    const classes = useStyles();
    const [currentUser, setCurrentUser] = useState(authStorage.getAuthUser());

    const onLogout = () => {
        logout();
        setCurrentUser(null);
        EventRegister.dispatch(ALERT_EVENT, Alerts.info('Logged out'));
    };

    const loadCurrentUser = () => {
        userLoader.load(user => setCurrentUser(user));
    };

    useEffect(() => {
        userLoader.load(user => setCurrentUser(user));
    }, []);

    return (
        <React.Fragment>
            <CssBaseline />
            <TopBar className={classes.root} user={currentUser} onLogout={onLogout} {...props}/>

            <main>
                <Container maxWidth={false} >
                    <CssBaseline />
                    <Switch>
                        <Route exact path={PATH_HOME} component={Home} />
                        <Route path={PATH_PROJECTS_SVG} component={AnimatedSvgProject} />
                        <Route path={PATH_PROJECTS} component={Projects} />
                        <Route path={PATH_ABOUT} component={About} />
                        <Route path={PATH_OAUTH2_LOGIN} render={(props) => <OAuth2RedirectHandler onRedirect={loadCurrentUser} {...props}/>} />
                        <Route path={PATH_CONFIRM + PATH_PASSWORD_RESET} render={(props) => <PasswordResetConfirm {...props}/>} />
                        <Route path={PATH_CONFIRM + PATH_REGISTER} render={(props) => <RegisterConfirm {...props}/>} />

                        <RedirectRoute component={SignUp}
                                       onLogin={loadCurrentUser}
                                       path={PATH_REGISTER}
                                       redirectPath={PATH_PROFILE}
                                       redirect={currentUser}
                                       {...props}
                        />
                        <RedirectRoute component={Profile}
                                       isLoading={false}
                                       user={currentUser}
                                       onLogout={onLogout}
                                       path={PATH_PROFILE}
                                       redirectPath={PATH_LOGIN}
                                       redirect={!currentUser}
                                       {...props}
                        />
                        <RedirectRoute component={SignIn}
                                       onLogin={loadCurrentUser}
                                       path={PATH_LOGIN}
                                       redirectPath={PATH_PROFILE}
                                       redirect={currentUser}
                                       {...props}
                        />
                        <RedirectRoute component={PasswordReset}
                                       path={PATH_PASSWORD_RESET}
                                       redirectPath={PATH_PROFILE}
                                       redirect={currentUser}
                                       {...props}
                        />
                        <RedirectRoute component={PasswordChange}
                                       user={currentUser}
                                       path={PATH_PASSWORD_CHANGE}
                                       redirectPath={PATH_LOGIN}
                                       redirect={!currentUser}
                                       {...props}
                        />
                        <Route component={NotFound} />
                    </Switch>
                </Container>
            </main>
            <AppFooter />
            <SnackbarAlerts/>
            <DialogAlerts/>
            <LeftDrawer />
        </React.Fragment>
    );
}