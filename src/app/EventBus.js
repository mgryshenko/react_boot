export const TOGGLE_DRAWER_EVENT = 'toggleDrawerEvent';
export const ALERT_EVENT = 'alertEvent';
export const DIALOG_EVENT = 'dialogEvent';
export const REFRESH_TOKEN_EVENT = 'refreshTokenEvent';

export const EventRegister = {
    events: {},
    dispatch: function (event, data) {
        if (!this.events[event]) return;
        const promise = new Promise(resolve => resolve());
        this.events[event].forEach(callback => promise.then(() => callback(data)));
    },
    subscribe: function (event, callback) {
        if (!this.events[event]) this.events[event] = [];
        this.events[event].push(callback);
        return {event: event, callback: callback};
    },
    unsubscribe: function (listener) {
        if (!this.events[listener.event]) return;
        const i = this.events[listener.event].indexOf(listener.callback);
        if (i > -1) this.events[listener.event].splice(i, 1);
    }
};