import React from 'react';
import {makeStyles} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";

const useStyles = makeStyles(theme => ({
    container: {
        marginTop: theme.spacing(3),
        padding: theme.spacing(3, 2),
        alignItems: 'center',
    },
    margin: {
        marginTop: theme.spacing(10),
    },
}));

export default function About() {
    const classes = useStyles();

    return (
        <Paper className={classes.container} elevation={0} >
            <Typography component="h3" variant="h5" gutterBottom>
                About this project
            </Typography>
            <Typography component="h4" variant="h6" gutterBottom>
                Intentions
            </Typography>
            <Typography variant="body1" gutterBottom>
                This app is the sandbox for developing and testing simple features of modern web applications.
                Communication with RESTful API server, security integration, social login, reactive UI -
                'must have' for web-app developers. As you may notice, any feature of modern web-app has been already invented,
                developed and re-invented by developers hundred times. Even more, these features are freely
                available today, as ready solutions or code snippets. IMHO, it is more important having complete
                understanding how 'simple' feature works. What is under the 'hood'. Why it is designed this way or
                another. What is this hype about. So that, the only value of this web-app is self education
            </Typography>
            <Typography component="div" gutterBottom>
                <Box fontStyle="oblique" variant="body1" >
                    TL;DR - this is yet-another useless pet project
                </Box>
            </Typography>
            <Typography component="h4" variant="h6" gutterBottom>
                Personal data usage
            </Typography>
            <Typography variant="body1" gutterBottom>
                During sign up or further app usage you provide personal details - email, social profile, password.
                The only guarantee is that this data will be processed and persisted within this web app only.
                Be ready that data could be lost/deleted at any time, as this app is under development. It is very
                common to have database drops as the new feature released. Also, despite production-grade security
                applied, there are always tiny chance for your data to be exposed to malicious users. Your account
                details also accessible to app developers and database superusers. That's why you can always delete
                all your personal data within profile settings. That's why during signing up suggested not to
                provide sensible data.
            </Typography>
            <Typography component='div' gutterBottom>
                <Box fontStyle="oblique" variant="body1" >
                    TL;DR - use fake mail accounts during registration (I'm sure you have one or two for spam and ads)
                </Box>
            </Typography>
            <Typography component="h4" variant="h6" gutterBottom>
                Contacts
            </Typography>
            <Typography variant="body1" gutterBottom>
                Currently there are no feedback system. No one is planned yet
            </Typography>
        </Paper>
    );
}
