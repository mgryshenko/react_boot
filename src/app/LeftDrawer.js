import React, {useEffect, useState} from 'react';
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import {Link} from "react-router-dom";
import HomeIcon from '@material-ui/icons/Home';
import AppsIcon from '@material-ui/icons/Apps';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import {makeStyles} from "@material-ui/core/styles";
import {PATH_ABOUT, PATH_HOME, PATH_PROJECTS} from "./AppRoutes";
import {EventRegister, TOGGLE_DRAWER_EVENT} from './EventBus'

const useStyles = makeStyles({
    list: {
        width: 250,
    },
});

const items = {
    home: {
        link: PATH_HOME,
        key: 'Home',
        desc: 'Go home',
        icon: <HomeIcon />,
    },
    projects: {
        link: PATH_PROJECTS,
        key: 'Projects',
        desc: 'Projects',
        icon: <AppsIcon />,
    },
    about: {
        link: PATH_ABOUT,
        key: 'About',
        desc: 'About',
        icon: <HelpOutlineIcon />,
    },
};

function DrawerListItem(props) {
    return (
        <ListItem
            component={ Link } to={props.item.link}
            button
            key={props.item.key}
            aria-label={props.item.desc}
            title={props.item.desc}
        >
            <ListItemIcon>
                {props.item.icon}
            </ListItemIcon>
            <ListItemText primary={props.item.desc}  />
        </ListItem>
    );
}

export default function LeftDrawer() {
    const classes = useStyles();
    const [opened, setOpened] = useState(false);

    const toggleDrawer = (open) => event => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setOpened(open);
    };

    useEffect(() => {
        const listener = EventRegister.subscribe(TOGGLE_DRAWER_EVENT, (open) => setOpened(open));
        return () => EventRegister.unsubscribe(listener);
    }, []);

    return (
        <div>
            <Drawer open={opened} onClose={toggleDrawer(false)} >
                <div
                    className={classes.list}
                    role="presentation"
                    onClick={toggleDrawer(false)}
                    onKeyDown={toggleDrawer(false)}
                >
                    <List>
                        <DrawerListItem item={items.home} />
                        <Divider />
                        <DrawerListItem item={items.projects} />
                        <DrawerListItem item={items.about} />
                    </List>
                </div>
            </Drawer>
        </div>
    );
}