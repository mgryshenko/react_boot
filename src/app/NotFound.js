import React from 'react';
import {Link} from "react-router-dom";
import HomeIcon from "@material-ui/icons/Home";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import {makeStyles} from "@material-ui/core/styles";
import {PATH_HOME} from "./AppRoutes";

const useStyles = makeStyles(theme => ({
    margin: {
        marginTop: theme.spacing(6),
    },
}));

export default function NotFound() {
    const classes = useStyles();

    return (
        <div >
            <Typography
                className={classes.margin}
                component="h1"
                variant="h1"
                align="center"
            >
                404
            </Typography>
            <Typography
                component="h2"
                variant="h5"
                align="center"
            >
                The Page you're looking for was not found.
            </Typography>
            <Container maxWidth="sm" align="center" className={classes.margin} >
                <Button
                    component={ Link } to={PATH_HOME}
                    size="large"
                    aria-label="Sign in or register"
                    title="Sign in or register"
                    startIcon={<HomeIcon />}
                >
                    Home
                </Button>
            </Container>
        </div>
    );
}