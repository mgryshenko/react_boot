import React from 'react';
import {Route, Redirect} from "react-router-dom";

export const PATH_HOME = '/';
export const PATH_LOGIN = '/login';
export const PATH_REGISTER = '/register';
export const PATH_PASSWORD_RESET = '/password';
export const PATH_PASSWORD_CHANGE = '/password_change';
export const PATH_PROFILE = '/profile';
export const PATH_ABOUT = '/about';
export const PATH_OAUTH2_LOGIN = '/oauth2/login/redirect';
export const PATH_PROJECTS = '/projects';
export const PATH_PROJECTS_SVG = '/projects/animated_svg';

export const PATH_LOCATION = 'http://' + window.location.host;
export const PATH_CONFIRM = '/confirm';
export const PATH_CONFIRM_REGISTER = PATH_LOCATION + PATH_CONFIRM + PATH_REGISTER;
export const PATH_CONFIRM_RESET_PASSWORD = PATH_LOCATION + PATH_CONFIRM + PATH_PASSWORD_RESET;

export const parseUrlParameter = (name, props) => {
    name = name.replace(/[[]/, '\\[').replace(/[\]]/, '\\]');
    const regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    const results = regex.exec(props.location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

export function RedirectRoute ({ component: Component, redirect, redirectPath, ...rest }) {
    if (redirect) {
        const toPath = redirectPath ? redirectPath : PATH_HOME;
        return <Route {...rest} render={props => <Redirect to={{ pathname: toPath, state: {from: props.location} }} /> } />;
    }
    return <Route {...rest} render={props => <Component {...rest} {...props} /> } />;
}