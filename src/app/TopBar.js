import React, {useState} from 'react';
import {makeStyles} from "@material-ui/core";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import MenuIcon from '@material-ui/icons/Menu';
import Button from "@material-ui/core/Button";
import {Link} from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import AppBar from "@material-ui/core/AppBar";
import Avatar from "@material-ui/core/Avatar";
import {PATH_HOME, PATH_LOGIN, PATH_PROFILE} from "./AppRoutes";
import {DIALOG_EVENT, EventRegister, TOGGLE_DRAWER_EVENT} from './EventBus'
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import {DialogAlertBuilder} from "../alerts/DialogModel";
import Box from "@material-ui/core/Box";

const useStyles = makeStyles(theme => ({
    avatar: {
        margin: 5,
        color: '#000',
        backgroundColor: '#fff',
        cursor: 'pointer',
    },
    toolbar: {
        borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbarTitle: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
}));

function ProfileButton(props) {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState(null);

    const openMenu = clickEvent => {
        setAnchorEl(clickEvent.currentTarget);
    };
    const closeMenu = () => {
        setAnchorEl(null);
    };

    const onClickLogout = () => {
        closeMenu();
        const dialog = new DialogAlertBuilder()
            .withTitle("Sign out?")
            .withContentText("You always can sign in back later")
            .withAction("Sign out", props.onLogout)
            .build();
        EventRegister.dispatch(DIALOG_EVENT, dialog);
    };

    return (
        <React.Fragment>
            <Avatar
                aria-label="Profile options"
                title="Profile options"
                alt="Profile options"
                className={classes.avatar}
                src={props.user.imageUrl}
                onClick={openMenu}
            >
                {!props.user.imageUrl && <AccountCircleIcon fontSize='large'/>}
            </Avatar>
            <Menu
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={closeMenu}
            >
                <MenuItem
                    component={ Link } to={PATH_PROFILE}
                    title='Go to profile page'
                    key='profile'
                    onClick={closeMenu}
                >
                    Profile
                </MenuItem>
                <MenuItem
                    title='Log out'
                    key='logout'
                    onClick={onClickLogout}
                >
                    Log out
                </MenuItem>
            </Menu>
        </React.Fragment>
    );
}

export default function TopBar(props) {
    const classes = useStyles();
    const openDrawer = () => EventRegister.dispatch(TOGGLE_DRAWER_EVENT, true);

    return (
        <AppBar color='inherit' position="static">
            <Toolbar variant="dense" className={classes.toolbar}>
                <IconButton
                    aria-label="Menu"
                    title="Menu"
                    size='medium'
                    onClick={openDrawer}
                >
                    <MenuIcon />
                </IconButton>
                <Typography
                    component="h2"
                    variant="h5"
                    color="inherit"
                    noWrap
                    className={classes.toolbarTitle}
                    aria-label="Home page"
                    title="Home page"
                >
                    <Box
                        fontWeight="fontWeightLight"
                        fontFamily="Monospace"
                        fontSize="h4.fontSize"
                    >
                        <Link to={PATH_HOME} style={{ textDecoration: 'none', color: 'unset' }}>
                            MG_boot
                        </Link>
                    </Box>

                </Typography>
                {!props.user &&
                    <Button
                        component={ Link } to={PATH_LOGIN}
                        aria-label="Sign in or register"
                        title="Sign in or register"
                        startIcon={<LockOutlinedIcon />}
                    >
                        Sign in
                    </Button>
                }
                {props.user &&
                    <ProfileButton onLogout={props.onLogout} {...props}/>
                }
            </Toolbar>
        </AppBar>

    );
}