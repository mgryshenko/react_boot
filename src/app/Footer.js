import React from 'react';
import {makeStyles} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";

function CopyrightTypography(props) {
    return <Typography variant="body2" color="textSecondary" align="center" {...props}/>
}

const useStyles = makeStyles(theme => ({
    footer: {
        backgroundColor: theme.palette.background.paper,
        marginTop: theme.spacing(8),
        padding: theme.spacing(6, 0),
    },
}));

export default function AppFooter() {
    const classes = useStyles();

    return(
        <footer className={classes.footer}>
            <CopyrightTypography>
                {'No copyright © '}
                Spring + React
                {' '}
                {new Date().getFullYear()}
            </CopyrightTypography>
            <CopyrightTypography>
                {'v' + process.env.REACT_APP_VERSION}
            </CopyrightTypography>
        </footer>
    );
}