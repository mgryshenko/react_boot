import React, {useState} from 'react';
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import {makeStyles} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import Fade from "@material-ui/core/Fade";
import {PATH_LOGIN, PATH_PASSWORD_CHANGE} from "../app/AppRoutes";
import {Templates} from "../auth/ApiTemplates";
import {DialogAlertBuilder} from "../alerts/DialogModel";
import {ALERT_EVENT, DIALOG_EVENT, EventRegister} from "../app/EventBus";
import {Alerts} from "../alerts/AlertModel";
import {Link} from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

const deleteAccountDialog = {
    title: "Delete your account?",
    text: "All your personal data will be deleted. All data associated with your account will be deleted as well, if it's not shared with other users. This action cannot be reverted, it will be performed immediately",
    actionName: "Delete immediately"
};

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        padding: theme.spacing(3, 2),
        alignItems: 'center',
    },
    textSecondary: {
        color: theme.palette.text.secondary,
    },
    avatar: {
        width: "100%",
        maxWidth: 210,
        height: 200,
    },
    item: {
        color: "#878787",
        width: "100%",
        maxWidth: 210,
        height: 200,
    },
    borderless: {
        margin: 0,
        padding: 0,
    }
}));

export default function Profile(props) {
    const classes = useStyles();
    const [deleting, setDeleting] = useState(false);

    const onDeleteClicked = () => {
        const confirm = new DialogAlertBuilder()
            .withTitle(deleteAccountDialog.title)
            .withContentText(deleteAccountDialog.text)
            .withAction(deleteAccountDialog.actionName, onDeleteConfirm)
            .build();
        EventRegister.dispatch(DIALOG_EVENT, confirm);
    };

    const onDeleteConfirm = () => {
        setDeleting(true);
        Templates.deleteMe()
            .then(onDelete)
            .catch((error) => {
                console.log('[Profile][onDeleteConfirm] error: ' + (error && JSON.stringify(error)));
                setDeleting(false);
                EventRegister.dispatch(ALERT_EVENT, Alerts.error("Couldn't delete your account. Try again later"));
            });
    };

    const onDelete = () => {
        props.onLogout();
        props.history.push(PATH_LOGIN);
    };

    const onLogoutClicked = () => {
        const dialog = new DialogAlertBuilder()
            .withTitle("Sign out?")
            .withContentText("You always can sign in back later")
            .withAction("Sign out", props.onLogout)
            .build();
        EventRegister.dispatch(DIALOG_EVENT, dialog);
    };

    return (
        <React.Fragment>
            <Container maxWidth="md">
                {props.isLoading &&
                <Container align='center'>
                    <Fade in={props.isLoading} style={{ transitionDelay: props.isLoading ? '800ms' : '0ms', }} unmountOnExit>
                        <CircularProgress />
                    </Fade>
                </Container>
                }
                {props.user &&
                <Paper className={classes.paper} elevation={6} >
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <Typography component="h3" variant="h5">Profile</Typography>
                        </Grid>
                        <Grid item lg={3} sm={4} xs={12}>
                            {!props.user.imageUrl &&
                                <AccountCircleIcon className={classes.item} fontSize='large'/>
                            }
                            {props.user.imageUrl &&
                                <Card className={classes.avatar} elevation={3}>
                                        <CardMedia className={classes.avatar} image={props.user.imageUrl} title="Avatar image" />
                                </Card>
                            }
                        </Grid>
                        <Grid item lg={9} sm={8} xs={12}>
                            <Typography component="h3" variant="h5">{props.user.name}</Typography>
                            <Typography component="p" className={classes.textSecondary}>{props.user.email}</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography component="h3" variant="h5">Settings</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <List component="nav" className={classes.root} aria-label="mailbox folders">
                                <Divider light />
                                <ListItem button divider component={ Link } to={PATH_PASSWORD_CHANGE} disabled={deleting}>
                                    <ListItemText
                                        primary="Change password"
                                        secondary="Shows password changing page"
                                    />
                                </ListItem>
                                <ListItem button divider color="secondary" onClick={onDeleteClicked} disabled={deleting}>
                                    <ListItemText
                                        primary="Delete my account" primaryTypographyProps={{color: 'secondary'}}
                                        secondary="Opens popup with further instructions"
                                    />
                                </ListItem>
                            </List>
                        </Grid>
                        <Grid item sm={12} xs={12} >
                            <Container align='center'>
                                <Button
                                    disabled={deleting}
                                    variant='outlined'
                                    color="primary"
                                    size="large"
                                    aria-label="Log out"
                                    title="Log out"
                                    align='center'
                                    onClick={onLogoutClicked}
                                >
                                    Log out
                                </Button>
                            </Container>
                        </Grid>
                    </Grid>
                </Paper>
                }
            </Container>
        </React.Fragment>
    );
}