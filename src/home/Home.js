import React from 'react';
import AnimatedCircles from "./AnimatedCircles";
import {makeStyles} from "@material-ui/core";
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import {PATH_PROJECTS} from "../app/AppRoutes";

const useStyles = makeStyles(theme => ({
    margin: {
        marginTop: theme.spacing(10),
    },
}));

export default function Home() {
    const classes = useStyles();

    return (
        <React.Fragment>
            <AnimatedCircles />
            <Container className={classes.margin} maxWidth='xs' align='center'>
                <Button
                    component={ Link } to={PATH_PROJECTS}
                    variant='outlined'
                    color="primary"
                    aria-label="Projects list"
                    title="Get started"
                    align='center'
                >
                    Get started
                </Button>
            </Container>
        </React.Fragment>
    );
}
