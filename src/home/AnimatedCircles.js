import React from 'react';
import {makeStyles} from "@material-ui/core";
import Container from "@material-ui/core/Container";

const useStyles = makeStyles(theme => ({
    margin: {
        marginTop: theme.spacing(6),
    },
    graphContainer: {
        width: '100%',
        height: '310px',
        overflow: 'hidden',
        perspective: '2000px',
        opacity: .7,
    },
    graphLayout: {
        height: '100%',
        margin: 'auto',
        position: 'relative',
        perspective: '2000px',
    },

    ...graphCircleChild('graphCircle1', 135, 135, '#70b000', 5),
    ...graphCircleChild('graphCircle2', 45, 45, '#ec412c', 4.5),
    ...graphCircleChild('graphCircle3', 45, 0, '#fcbd00', 4),
    ...graphCircleChild('graphCircle4', 135, 0, '#2da94f', 3.5),
    ...graphCircleChild('graphCircle5', 100, 0, '#f57700', 3),
    ...graphCircleChild('graphCircle6', 0, -105, '#2098f3', 2.5),
    ...graphCircleChild('graphCircle7', 45, 45, '#30bbb0', 2),
    ...graphCircleChild('graphCircle8', 135, 135, '#ff453c', 1.5),
    ...graphCircleChild('graphCircle9', 115, 113, '#2098f3', 3),
    ...graphCircleChild('graphCircle10', -45, -45, '#2098f3', 2),
}));

function graphCircleChild(className, x, y, color, sec) {
    function animationName(title) {
        return title + 'Anim';
    }

    return {
        [className]: {
            position: 'absolute',
            top: '50%',
            left: '50%',
            width: '310px',
            height: '310px',
            borderRadius: '50%',
            borderLeft: 'none',
            borderTop: 'none',
            transform: 'translate(-50%,-50%) rotateX(' + x + 'deg) rotateY(' + y + 'deg) rotate(0deg)',
            border: '5px solid ' + color,
            animation: '$' + animationName(className) + ' ' + sec + 's infinite alternate linear',
        },
        ['@keyframes ' + animationName(className)]: {
            to: {
                borderRadius: '50%',
                transform: 'translate(-50%,-50%) rotateX(' + x + 'deg) rotateY(' + y + 'deg) rotate(1turn)',
                opacity: 0,
            }
        }
    }
}

export default function AnimatedCircles() {
    const classes = useStyles();

    return (
        <Container maxWidth="sm" align="center" className={classes.margin} >
            <div className={classes.graphContainer}>
                <div className={classes.graphLayout}>
                    <div className={classes.graphCircle1} />
                    <div className={classes.graphCircle2} />
                    <div className={classes.graphCircle3} />
                    <div className={classes.graphCircle4} />
                    <div className={classes.graphCircle5} />
                    <div className={classes.graphCircle6} />
                    <div className={classes.graphCircle7} />
                    <div className={classes.graphCircle8} />
                </div>
            </div>
        </Container>
    );
}
